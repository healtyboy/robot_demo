*** Settings ***
Resource          ../../SETTINGS/AllResource.robot

*** Keywords ***
ISO_Auth
    Comment    Set Library Search Order    SeleniumLibrary
    Set Suite Variable    ${FolderData}    ISO
    [ER] Get Path Test Data on Configuration    ${FileConfigPath}${FolderData}${/}${FolderData}_Configuration.xlsx    ${ConfigSheetName}    ROBOT_S_${FolderData}_Auth
    Run Keyword If    "${CAPTURE_FLAG}" == "Y"    CAPTURE SCREEN SET
    [EW] Write Cell ISO Clear Rows Data To Excel    ${OuputSheetName}
    @{AllListData}    [ER] Get Data All Row For Excel    ${TEST_DATA}    ${InputSheetName}
    ${countAllListData}    Get Length    ${AllListData}
    Log to Console    CountAllListData --> ${countAllListData}
    Set Suite Variable    ${AddRow}    1
    Set Suite Variable    ${CAPTURE_ROW}    2
    FOR    ${i}    IN RANGE    1    ${countAllListData}
        Run Keyword If    '${VERIFY_FLAG}' == 'Y' and '${EXIST_FLAG}' == 'Y'    Run Keyword If Test Failed
        ${TAG}    Get From Dictionary    ${AllListData}[${i}]    TAG
        Run Keyword If    '${TAG}' != 'Run'    Continue For Loop
        START TRANSACTION TIME
        Set Suite Variable    ${RUNNING_STEP}    1
        ${PrimaryAccountNumber}    Get From Dictionary    ${AllListData}[${i}]    PrimaryAccountNumber
        ${FixFild}    Get From Dictionary    ${AllListData}[${i}]    FixFild
        ${Terminal}    Get From Dictionary    ${AllListData}[${i}]    Terminal
        ${Merchant}    Get From Dictionary    ${AllListData}[${i}]    Merchant
        ${AmountTransaction}    Get From Dictionary    ${AllListData}[${i}]    AmountTransaction
        ${CalChipData}    Get From Dictionary    ${AllListData}[${i}]    CalChipData
        ${ChipData}    Get From Dictionary    ${AllListData}[${i}]    ChipData
        ${ServiceCode}    Get From Dictionary    ${AllListData}[${i}]    ServiceCode
        ${Pin}    Get From Dictionary    ${AllListData}[${i}]    Pin
        ${CVV}    Get From Dictionary    ${AllListData}[${i}]    CVV
        ${Track2}    Get From Dictionary    ${AllListData}[${i}]    Track2
        ${ExpireDate}    Get From Dictionary    ${AllListData}[${i}]    ExpireDate
        Set Suite Variable    ${MSG}    ${PrimaryAccountNumber}${FixFild}${Terminal}${Merchant}${AmountTransaction}${CalChipData}${ChipData}${ServiceCode}${Pin}${CVV}${Track2}${ExpireDate}
        Comment    ${iso}    [I] ISO8583    ${MSG}
        Comment    ${STATUS}    [S] Client Socket    192.168.43.165    5000    1024    ${iso}
        Set Suite Variable    ${TS_STATUS}    Pass
        Set Suite Variable    ${TS_MESSAGE}    ${EMPTY}
        [EW] Write Cell ISO Auth To Excel    OUTPUT    ${AddRow}    ${PrimaryAccountNumber}    ${FixFild}    ${Terminal}    ${Merchant}    ${AmountTransaction}    ${CalChipData}    ${ChipData}    ${ServiceCode}    ${Pin}    ${CVV}    ${Track2}    ${ExpireDate}
        Set Suite Variable    ${CAPTURE_ROW}    ${${CAPTURE_ROW} + 1}
        Run Keyword If    '${VERIFY_FLAG}' == 'Y' and '${TS_STATUS}' == "FAIL"    Set Suite Variable    ${EXIST_FLAG}    Y
    END

Set Message
    [Arguments]    ${row}    ${countRow}
    ${msg}=    Set Variable    ${row}[0]
    FOR    ${j}    IN RANGE    1    ${countRow}
        ${msg}=    Set Variable    ${msg}${row}[${j}]
    END
    [Return]    ${msg}
