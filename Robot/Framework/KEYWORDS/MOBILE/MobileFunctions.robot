*** Settings ***
Resource          ../../SETTINGS/StandardLibrary.robot
Resource          ../../SETTINGS/AllResource.robot

*** Keywords ***
[M] Capture Page Screenshot
    [Arguments]    ${FOLDER_FUNCTION_NAME}    ${TRANS_NAME}
    Set Library Search Order    AppiumLibrary
    ${DateTime}    Get Current Date    result_format=%Y%m%d%H%M%S%f
    ${ScreenshotDate}    Convert Date    ${DateTime}    result_format=%Y%m%d%H%M%S
    Log To Console    Screenshot Date --> ${ScreenshotDate}
    ${FOLDER_ROW}    ADD ZERO    ${CAPTURE_ROW}
    ${RUNNING}    ADD ZERO    ${RUNNING_STEP}
    Create Directory    ${FOLDER_CAPTURE_DATE}${/}${FOLDER_FUNCTION_NAME}${/}${FOLDER_ROW}
    ${OutputScreenshot}    Set Variable    ${FOLDER_CAPTURE_DATE}${/}${FOLDER_FUNCTION_NAME}${/}${FOLDER_ROW}${/}${RUNNING}_${TRANS_NAME}_${ScreenshotDate}.png
    Log To Console    FOLDER_CAPTURE_DATE__${FOLDER_CAPTURE_DATE}
    Log To Console    FOLDER_FUNCTION_NAME ---> ${FOLDER_FUNCTION_NAME}
    Log To Console    FOLDER_ROW--> ${FOLDER_ROW}
    Log To Console    OutputScreenshot --> ${OutputScreenshot}
    Sleep    1s
    AppiumLibrary.Capture Page Screenshot    ${OutputScreenshot}
    Set Suite Variable    ${RUNNING_STEP}    ${${RUNNING_STEP} + 1}
    Set Suite Variable    ${CAPTURE_SCREEN_SHOT}    ${FOLDER_CAPTURE_DATE}${/}${FOLDER_FUNCTION_NAME}${/}${FOLDER_ROW}
    [Return]    ${OutputScreenshot}

[M] Check Element
    [Arguments]    ${stepName}    ${ElementCheck}    ${timeout}=10s
    Set Library Search Order    AppiumLibrary
    Comment    KEYWORD STATUS PENDING    ${stepName}
    ${Status}    ${Error}    Run Keyword And Ignore Error    Wait Until Page Contains Element    ${ElementCheck}    timeout=${timeout}
    Set Suite Variable    ${TS_STATUS}    ${Status}
    Set Suite Variable    ${TS_MESSAGE}    ${Error}
    Log To Console    ${stepName} Status : ${Status}
    Log To Console    ${stepName} Error : ${Error}
    Comment    ${ScreenShot}    [M] Capture Page Screenshot
    Comment    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]

[M] Check Message
    [Arguments]    ${stepName}    ${Value}    ${timeout}=10s
    Set Library Search Order    AppiumLibrary
    Comment    KEYWORD STATUS PENDING    ${stepName}
    ${Status}    ${Error}    Run Keyword And Ignore Error    Wait Until Page Contains    ${Value}    timeout=${timeout}
    Set Suite Variable    ${TS_STATUS}    ${Status}
    Set Suite Variable    ${TS_MESSAGE}    ${Error}
    Log To Console    ${stepName} Status : ${TS_STATUS}
    Log To Console    ${stepName} Error : ${TS_MESSAGE}
    Comment    ${ScreenShot}    [M] Capture Page Screenshot
    Comment    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]

[M] Check Message Element
    [Arguments]    ${Element}    ${Value}
    Set Library Search Order    AppiumLibrary
    Comment    KEYWORD STATUS PENDING    ${stepName}
    WAIT FOR PAGE CONTAINS ELEMENT    ${Element}
    ${Session}    Get Webelement    ${Element}
    ${GetMessage}    Get Text    ${Session}
    ${Text}    Set Variable If    '${GetMessage}'=='${Value}'    PASS    FAIL
    ${MessageOut}    Set Variable    '${GetMessage}' and '${Value}'
    ${Return}    Create List    ${Text}    ${MessageOut}
    Comment    KEYWORD STATUS END
    [Teardown]
    [Return]    ${Return}

[M] Check Message Position
    [Arguments]    ${Value}    ${Positions}=1
    Set Library Search Order    AppiumLibrary
    Comment    KEYWORD STATUS PENDING    ${stepName}
    ${Return}    WAIT FOR PANG CONTAINS    ${Value}
    ${Session}    Get Webelements    //*[@*='${Value}']
    Insert Into List    ${Session}    0    ${EMPTY}
    ${GetText}    Get Text    @{Session}[${Positions}]
    ${Text}    Set Variable If    '${GetText}'=='${Value}'    PASS    FAIL
    ${ValueOut}    Set Variable    '${GetText}' and '${Value}'
    ${Return}    Create List    ${Text}    ${ValueOut}
    Comment    KEYWORD STATUS END
    [Teardown]
    [Return]    ${Return}

[M] Click Element
    [Arguments]    ${stepName}    ${ElementClick}
    Set Library Search Order    AppiumLibrary
    Comment    KEYWORD STATUS PENDING    ${stepName}
    WAIT FOR PAGE CONTAINS ELEMENT    ${ElementClick}
    ${GetSession}    Get Webelement    ${ElementClick}
    ${Status}    ${Error}    Run Keyword And Ignore Error    Click Element    ${GetSession}
    Comment    ${Status}    ${Error}    Run Keyword And Ignore Error    Click Element    ${ElementClick}
    Set Suite Variable    ${TS_STATUS}    ${Status}
    Set Suite Variable    ${TS_MESSAGE}    ${Error}
    Log To Console    ${stepName} Status : ${TS_STATUS}
    Log To Console    ${stepName} Error : ${TS_MESSAGE}
    Comment    ${ScreenShot}    [M] Capture Page Screenshot
    Comment    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]

[M] Click Element Position
    [Arguments]    ${stepName}    ${ElementClick}    ${Position}
    Set Library Search Order    AppiumLibrary
    Comment    KEYWORD STATUS PENDING    ${stepName}
    WAIT FOR PAGE CONTAINS ELEMENT    ${ElementClick}
    ${GetSession}    Get Webelements    ${ElementClick}
    Insert Into List    ${GetSession}    0    ${EMPTY}
    ${Status}    ${Error}    Run Keyword And Ignore Error    Click Element    @{GetSession}[${Position}]
    Set Suite Variable    ${STEP_STATUS}    ${Status}
    Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
    Log To Console    ${stepName} Status : ${Status}
    Log To Console    ${stepName} Error : ${Error}
    Comment    ${ScreenShot}    [M] Capture Page Screenshot
    Comment    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]

[M] Element Get Count
    [Arguments]    ${ElementGetCount}
    Set Library Search Order    AppiumLibrary
    Comment    KEYWORD STATUS PENDING    ${stepName}
    WAIT FOR PAGE CONTAINS ELEMENT    ${ElementGetCount}
    ${ListAllPage}    Get WebElements    ${ElementGetCount}
    ${CountElement}    Get Length    ${ListAllPage}
    ${Return}    Set Variable    ${CountElement}
    Comment    KEYWORD STATUS END
    [Teardown]
    [Return]    ${Return}

[M] Get Count Element
    [Arguments]    ${ElementGetCount}
    Set Library Search Order    AppiumLibrary
    Comment    KEYWORD STATUS PENDING    [M] Get Count Element
    WAIT FOR PAGE CONTAINS ELEMENT    ${ElementGetCount}
    ${ListAllPage}    Get WebElements    ${ElementGetCount}
    ${CountElement}    Get Length    ${ListAllPage}
    Comment    ${Return}    Set Variable If    '${CountElement}'=='${CountItem}'    PASS    FAIL
    Comment    KEYWORD STATUS END
    [Teardown]
    [Return]    ${CountElement}

[M] Get Message
    [Arguments]    ${Value}
    Set Library Search Order    AppiumLibrary
    Comment    KEYWORD STATUS PENDING    [M] Get Message
    WAIT FOR PANG CONTAINS    ${Value}
    ${ListAllPage}    Get WebElement    //*[contains(@text,'${Value}') or contains(@value,'${Value}')]
    ${GetText}    Get Text    ${ListAllPage}
    ${Return}    Set Variable    ${GetText}
    Comment    KEYWORD STATUS END
    [Teardown]
    [Return]    ${Return}

[M] Get Message Element
    [Arguments]    ${stepName}    ${ElementGetValue}
    Set Library Search Order    AppiumLibrary
    Comment    KEYWORD STATUS PENDING    [M] Get Message Element
    WAIT FOR PAGE CONTAINS ELEMENT    ${ElementGetValue}
    ${ListAllPage}    Get WebElement    ${ElementGetValue}
    ${GetText}    Get Text    ${ListAllPage}
    ${Return}    Set Variable    ${GetText}
    Run Keyword If    "${GetText}" != ""    Set Suite Variable    ${TS_STATUS}    PASS
    Run Keyword If    "${GetText}" != ""    Set Suite Variable    ${TS_MESSAGE}    ${EMPTY}
    Run Keyword If    "${GetText}" == ""    Set Suite Variable    ${TS_STATUS}    FAIL
    Run Keyword If    "${GetText}" == ""    Set Suite Variable    ${TS_MESSAGE}    Cannot Get Value
    Log To Console    ${stepName} Status : ${TS_STATUS}
    Log To Console    ${stepName} Error : ${TS_MESSAGE}
    Comment    KEYWORD STATUS END
    [Teardown]
    [Return]    ${Return}

[M] Get Message Element Position
    [Arguments]    ${ElementGetValue}    ${Position}
    Set Library Search Order    AppiumLibrary
    Comment    KEYWORD STATUS PENDING    [M] Get Message Element Position
    WAIT FOR PAGE CONTAINS ELEMENT    ${ElementGetValue}
    ${GetSession}    Get Webelements    ${ElementGetValue}
    Insert Into List    ${GetSession}    0    ${EMPTY}
    ${Return}    Get Text    @{GetSession}[${Position}]
    ${Return}    Set Variable    ${Return}
    Comment    KEYWORD STATUS END
    [Teardown]
    [Return]    ${Return}

[M] Get Message Regular
    [Arguments]    ${Value}    ${Regular}
    Set Library Search Order    AppiumLibrary
    Comment    KEYWORD STATUS PENDING    [M] Get Message Regular
    WAIT FOR PAGE CONTAINS ELEMENT    //*[contains(@text,'${Value}') or contains(@value,'${Value}')]    60s
    ${ListAllPage}    Get WebElement    //*[contains(@text,'${Value}') or contains(@value,'${Value}')]
    ${GetText}    Get Text    ${ListAllPage}
    ${GetText}    Get Regexp Matches    ${GetText}    ${Regular}
    ${Return}    Set Variable    ${GetText}
    Comment    KEYWORD STATUS END
    [Teardown]
    [Return]    ${Return}

[M] Go Back
    [Arguments]    ${stepName}
    Set Library Search Order    AppiumLibrary
    Comment    KEYWORD STATUS PENDING    ${stepName}
    ${Status}    ${Error}    Run Keyword And Ignore Error    Go Back
    Set Suite Variable    ${STEP_STATUS}    ${Status}
    Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
    Log To Console    ${stepName} Status : ${Status}
    Log To Console    ${stepName} Error : ${Error}
    Comment    ${ScreenShot}    [M] Capture Page Screenshot
    Comment    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]

[M] Go Back To Element
    [Arguments]    ${stepName}    ${Element}
    Set Library Search Order    AppiumLibrary
    Comment    KEYWORD STATUS PENDING    ${stepName}
    FOR    ${index}    IN RANGE    10
        ${Status}    ${Error}    WAIT FOR PAGE CONTAINS ELEMENT    ${Element}    0.5s
        Run Keyword If    '${Status}'!='PASS'    Go Back
        Set Suite Variable    ${STEP_STATUS}    ${Status}
        Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
        Run Keyword If    '${Status}'=='PASS'    Exit For Loop
    END
    Log To Console    ${stepName} Status : ${Status}
    Log To Console    ${stepName} Error : ${Error}
    Comment    ${ScreenShot}    [M] Capture Page Screenshot
    Comment    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]

[M] Go Back To Message
    [Arguments]    ${stepName}    ${Value}
    Set Library Search Order    AppiumLibrary
    Comment    KEYWORD STATUS PENDING    ${stepName}
    FOR    ${index}    IN RANGE    10
        ${Status}    ${Error}    WAIT FOR PANG CONTAINS    ${Value}    5s
        Run Keyword If    '${Status}'!='PASS'    Go Back
        Set Suite Variable    ${STEP_STATUS}    ${Status}
        Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
        Run Keyword If    '${Status}'=='PASS'    Exit For Loop
    END
    Log To Console    ${stepName} Status : ${Status}
    Log To Console    ${stepName} Error : ${Error}
    Comment    ${ScreenShot}    [M] Capture Page Screenshot
    Comment    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]

[M] Input Text
    [Arguments]    ${stepName}    ${ElementInputText}    ${Value}
    Set Library Search Order    AppiumLibrary
    Comment    KEYWORD STATUS PENDING    ${stepName}
    WAIT FOR PAGE CONTAINS ELEMENT    ${ElementInputText}
    ${GetSession}    Get Webelement    ${ElementInputText}
    ${Status}    ${Error}    Run Keyword And Ignore Error    Input Text    ${GetSession}    ${Value}
    Set Suite Variable    ${TS_STATUS}    ${Status}
    Set Suite Variable    ${TS_MESSAGE}    ${Error}
    Log To Console    ${stepName} Status : ${TS_STATUS}
    Log To Console    ${stepName} Error : ${TS_MESSAGE}
    Comment    ${ScreenShot}    [M] Capture Page Screenshot
    Comment    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]

[M] Input Text Position
    [Arguments]    ${stepName}    ${ElementInputText}    ${Position}    ${Value}
    Set Library Search Order    AppiumLibrary
    Comment    KEYWORD STATUS PENDING    ${stepName}
    WAIT FOR PAGE CONTAINS ELEMENT    ${ElementInputText}
    ${GetSession}    Get Webelements    ${ElementInputText}
    Insert Into List    ${GetSession}    0    ${EMPTY}
    ${Status}    ${Error}    Run Keyword And Ignore Error    Input Text    @{GetSession}[${Position}]    ${Value}
    Set Suite Variable    ${STEP_STATUS}    ${Status}
    Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
    Log To Console    ${stepName} Status : ${Status}
    Log To Console    ${stepName} Error : ${Error}
    Comment    ${ScreenShot}    [M] Capture Page Screenshot
    Comment    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]

[M] Manual Input
    [Arguments]    ${stepName}    ${ElementManual}    ${Value}
    Set Library Search Order    AppiumLibrary
    Comment    KEYWORD STATUS PENDING    ${stepName}
    WAIT FOR PAGE CONTAINS ELEMENT    ${ElementManual}
    ${GetSession}    Get Webelement    ${ElementManual}
    ${Status}    ${Error}    Run Keyword And Ignore Error    Click Element    ${GetSession}
    Set Suite Variable    ${STEP_STATUS}    ${Status}
    Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
    FOR    ${i}    IN    1
        ${Status}    ${Error}    Run Keyword And Ignore Error    Execute Manual Step    ${Value}
        Set Suite Variable    ${STEP_STATUS}    ${Status}
        Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
    END
    Log To Console    ${stepName} Status : ${Status}
    Log To Console    ${stepName} Error : ${Error}
    Comment    ${ScreenShot}    [M] Capture Page Screenshot
    Comment    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]

[M] Open Application
    [Arguments]    ${stepName}    ${Platform}    ${Version}    ${DevicesName}    ${Package}    ${Activity}    ${Port}=4723    ${Resetapp}=true
    Set Library Search Order    AppiumLibrary
    Comment    KEYWORD STATUS PENDING    ${stepName}
    ${ANDROID}    Convert To Lowercase    '${Platform}'=='ANDROID'
    ${iOS}    Convert To Lowercase    '${Platform}'=='iOS'
    Comment    Run Keyword And Ignore Error    Appium    ${Port}
    Run Keyword If    ${ANDROID}    Run Keyword    ANDROID SET    Android    ${Version}    ${DevicesName}    ${Package}    ${Activity}    ${Resetapp}    ${Port}
    Run Keyword If    ${iOS}    Run Keyword    iOS SET    iOS    ${Version}    ${DevicesName}    ${Package}    ${Activity}    ${Resetapp}    ${Port}
    Comment    ${ScreenShot}    [M] Capture Page Screenshot
    Comment    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]

[M] Swipe
    [Arguments]    ${stepName}    ${Element_Start}    ${Element_End}
    Set Library Search Order    AppiumLibrary
    Comment    KEYWORD STATUS PENDING    ${stepName}
    ${Start}    Get Element Location    ${Element_Start}
    ${End}    Get Element Location    ${Element_End}
    @{List}    Create List    ${Start['x']}    ${Start['y']}    ${End['x']}    ${End['y']}    2000
    ${Status}    ${Error}    Run Keyword And Ignore Error    Swipe    @{List}
    Comment    Set Suite Variable    ${STEP_STATUS}    ${Status}
    Comment    Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
    Comment    Log To Console    ${stepName} Status : ${Status}
    Comment    Log To Console    ${stepName} Error : ${Error}
    Comment    ${ScreenShot}    [M] Capture Page Screenshot
    Comment    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]

[M] Swipe By Percent Height
    [Arguments]    ${stepName}    ${Element_Start}    ${Element_End}
    Set Library Search Order    AppiumLibrary
    Comment    KEYWORD STATUS PENDING    ${stepName}
    ${height}    Get Window Height
    ${width}    Get Window Width
    ${StartX}    Evaluate    ${width}/2
    ${StartY}    Evaluate    (${height}*${Element_Start})/100
    ${EndX}    Evaluate    ${width}/2
    ${EndY}    Evaluate    (${height}*${Element_End})/100
    @{List}    Create List    ${StartX}    ${StartY}    ${EndX}    ${EndY}    1000
    ${Status}    ${Error}    Run Keyword And Ignore Error    Swipe    @{List}
    Set Suite Variable    ${STEP_STATUS}    ${Status}
    Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
    Log To Console    ${stepName} Status : ${Status}
    Log To Console    ${stepName} Error : ${Error}
    Comment    ${ScreenShot}    [M] Capture Page Screenshot
    Comment    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]

[M] Swipe By Percent Width
    [Arguments]    ${stepName}    ${Element_Start}    ${Element_End}
    Set Library Search Order    AppiumLibrary
    Comment    KEYWORD STATUS PENDING    ${stepName}
    ${height}    Get Window Height
    ${width}    Get Window Width
    ${StartX}    Evaluate    (${width}*${Element_Start})/100
    ${StartY}    Evaluate    ${height}/2
    ${EndX}    Evaluate    (${width}*${Element_End})/100
    ${EndY}    Evaluate    ${height}/2
    @{List}    Create List    ${StartX}    ${StartY}    ${EndX}    ${EndY}    1000
    ${Status}    ${Error}    Run Keyword And Ignore Error    Swipe    @{List}
    Set Suite Variable    ${STEP_STATUS}    ${Status}
    Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
    Log To Console    ${stepName} Status : ${Status}
    Log To Console    ${stepName} Error : ${Error}
    Comment    ${ScreenShot}    [M] Capture Page Screenshot
    Comment    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]

[M] Swipe Position
    [Arguments]    ${stepName}    ${Element}
    Set Library Search Order    AppiumLibrary
    Comment    KEYWORD STATUS PENDING    ${stepName}
    WAIT FOR PAGE CONTAINS ELEMENT    ${Element}
    ${ElementSize}    Get Element Size    ${Element}
    ${Position}    Get Element Location    ${Element}
    @{List}    Create List    ${ElementSize['width']}    ${Position['x']}    ${Position['y']}    ${Position['x']}    3000
    ${Status}    ${Error}    Run Keyword And Ignore Error    Swipe    @{List}
    Set Suite Variable    ${STEP_STATUS}    ${Status}
    Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
    Log To Console    ${stepName} Status : ${Status}
    Log To Console    ${stepName} Error : ${Error}
    Comment    ${ScreenShot}    [M] Capture Page Screenshot
    Comment    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]

[M] Swipe Up
    [Arguments]    ${stepName}    ${Element}
    Set Library Search Order    AppiumLibrary
    ${element_size}=    Get Element Size    ${Element}
    ${element_location}=    Get Element Location    ${Element}
    ${start_x}=    Evaluate    ${element_location['x']} + (${element_size['width']} * 0.5)
    ${start_y}=    Evaluate    ${element_location['y']} + (${element_size['height']} * 0.8)
    ${end_x}=    Evaluate    ${element_location['x']} + (${element_size['width']} * 0.5)
    ${end_y}=    Evaluate    ${element_location['y']} + (${element_size['height']} * 0.2)
    Log to Console    ${start_x} : ${start_y} : ${end_x} : ${end_y}
    Swipe    ${start_x}    ${start_y}    ${end_x}    ${end_y}    1000
    Sleep    1s
    Swipe    ${start_x}    ${start_y}    ${end_x}    ${end_y}    1000
    Sleep    1s
    [Teardown]

[M] Swipe Down
    [Arguments]    ${stepName}    ${Element}
    Set Library Search Order    AppiumLibrary
    ${element_size}=    Get Element Size    ${Element}
    ${element_location}=    Get Element Location    ${Element}
    ${start_x}=    Evaluate    ${element_location['x']} + (${element_size['width']} * 0.5)
    ${start_y}=    Evaluate    ${element_location['y']} + (${element_size['height']} * 0.2)
    ${end_x}=    Evaluate    ${element_location['x']} + (${element_size['width']} * 0.5)
    ${end_y}=    Evaluate    ${element_location['y']} + (${element_size['height']} * 0.8)
    Log to Console    ${start_x} : ${start_y} : ${end_x} : ${end_y}
    Swipe    ${start_x}    ${start_y}    ${end_x}    ${end_y}    1000
    Sleep    1s
    Swipe    ${start_x}    ${start_y}    ${end_x}    ${end_y}    1000
    Sleep    1s
    [Teardown]

[M] Get Messages Error
    [Arguments]    ${stepName}    ${ElementGetValue}    ${timeout}=10s
    Set Library Search Order    AppiumLibrary
    Comment    KEYWORD STATUS PENDING    ${stepName}
    WAIT FOR PAGE CONTAINS ELEMENT    ${ElementGetValue}
    ${ListAllPage}    Get WebElement    ${ElementGetValue}
    ${ERROR_MESSAGE}    Get Text    ${ListAllPage}
    Set Suite Variable    ${TS_MESSAGE}    ${ERROR_MESSAGE}

[M] Click Date Element
    [Arguments]    ${stepName}    ${ElementClick}
    Set Library Search Order    AppiumLibrary
    WAIT FOR PAGE CONTAINS ELEMENT    ${ElementClick}    10s
    Comment    ${GetSession}    Get Webelement    ${ElementClick}
    ${Status}    ${Error}    Run Keyword And Ignore Error    Click Element    ${ElementClick}
    Set Suite Variable    ${TS_STATUS}    ${Status}
    Set Suite Variable    ${TS_MESSAGE}    ${Error}
    Log To Console    ${stepName} Status : ${TS_STATUS}
    Log To Console    ${stepName} Error : ${TS_MESSAGE}
