*** Settings ***
Resource          ../../SETTINGS/StandardLibrary.robot
Resource          ../../SETTINGS/AllResource.robot

*** Keywords ***
KEYWORD STATUS END
    [Arguments]    ${ScreenShot}
    Set Suite Variable    ${KEYWORD_STATUS}    ${STEP_STATUS}
    Log to Console    CurrenRowStep --> ${currentRowStep}
    [EW] Write Cell EndStep To Excel    TestStep    ${currentRowStep}    ${KEYWORD_STATUS}    ${ScreenShot}
    Set Suite Variable    ${runningStep}    ${${runningStep} + 1}
    Set Suite Variable    ${currentRowStep}    ${${currentRowStep} + 1}
    Should Be Equal As Strings    "${STEP_STATUS}"    "PASS"

KEYWORD STATUS PENDING
    [Arguments]    ${KeywordName}
    Set Suite Variable    ${KEYWORD_STATUS}    FAIL
    Set Suite Variable    ${KEYWORD_NAME}    ${KeywordName}
    Log to Console    KEYWORD_NAME --> ${KEYWORD_NAME}
    FOR    ${i}    IN    ${KeywordName}
        log    [Pending] ${i}
        Log To Console    [Pending] ${i}
    END
    Log to Console    CurrenRowStep --> ${currentRowStep}
    [EW] Write Cell StartStep To Excel    TestStep    ${currentRowStep}    ${KEYWORD_NAME}

KEYWORD STATUS TEARDOWN
    FOR    ${i}    IN    ${KEYWORD_NAME}
        log    [${KEYWORD_STATUS}] ${i}
        Log To Console    [${KEYWORD_STATUS}] ${i}
    END

WAIT FOR PAGE CONTAINS ELEMENT
    [Arguments]    ${Element}    ${TimeOut}=10s
    ${Return}    Run Keyword And Ignore Error    Wait Until Page Contains Element    ${Element}    ${TimeOut}
    [Return]    ${Return}

WAIT FOR PAGE CONTAINS
    [Arguments]    ${Message}    ${TimeOut}=10s
    ${Return}    Run Keyword And Ignore Error    Wait Until Page Contains    ${Message}    ${TimeOut}
    [Return]    ${Return}

TEST CASE STATUS END
    [Arguments]    ${Msg}=PASS
    Set Suite Variable    ${TESTCASE_STATUS}    ${Msg}

TEST CASE STATUS PENDING
    [Arguments]    ${TestCase}    ${TestCaseName}
    Set Suite Variable    ${TESTCASE_STATUS}    FAIL
    Set Suite Variable    ${testCaseStep}    ${TestCase}
    Set Suite Variable    ${TESTCASE_NAME}    ${TestCaseName}
    Set Suite Variable    ${runningStep}    1
    FOR    ${i}    IN    ${TestCaseName}
        Log To Console    ========================================================================================================================================
        Log To Console    [Pending] ${i}
        Log To Console    ========================================================================================================================================
    END

TEST CASE STATUS TEARDOWN
    FOR    ${i}    IN    ${TESTCASE_NAME}
        Log To Console    ========================================================================================================================================
        Log To Console    [${KEYWORD_STATUS}] ${i}
        Log To Console    ========================================================================================================================================
    END

REPORT SET
    ${FileDate}    Get Current Date    result_format=%Y%m%d_%H%M%S
    ${RESULT_FORMAT}    Set Variable    SIUpgrade_${FileDate}
    ${folderDate}    Get Current Date    result_format=%Y%m%d_%H%M%S
    Log to Console    CURDIR --> ${CURDIR}
    ${DIR}    Replace String    ${CURDIR}    RF_Framework${/}KEYWORDS${/}GENERAL    RF_Framework
    ${DIR}    Replace String    ${DIR}    RF_Framework${/}KEYWORDS${/}GENERAL    RF_Framework
    Set Suite Variable    ${DIR}    ${DIR}
    Set Suite Variable    ${Foldername}    ${DIR}${/}TEST_RESULT${/}${folderDate}
    Create Directory    ${DIR}${/}TEST_RESULT${/}${folderDate}
    Log To Console    Create Directory --> ${DIR}${/}TEST_RESULT${/}${folderDate}
    [EW] Open Excel To Write    TEST_DATA/TestController.xlsx    TestController    ${RESULT_FORMAT}
    Set Suite Variable    ${RESULT}    ${RESULT_FORMAT}
    Set Suite Variable    ${Filename}    ${Foldername}${/}${RESULT}.xlsx
    Set Suite Variable    ${SheetName}    TestController
    Set Suite Variable    ${TOTAL_EXECUTED}    0
    Set Suite Variable    ${TOTAL_PASSED}    0
    Set Suite Variable    ${TOTAL_FAILED}    0
    Set Suite Variable    ${currentRowStep}    1
    Set Suite Variable    ${KEYWORD_NAME}    ${EMPTY}
    ${ExecutedDate}    Get Current Date    result_format=%d/%m/%Y
    Set Suite Variable    ${EXECUTED_DATE}    ${ExecutedDate}
    Set Suite Variable    ${ERROR_DESCRIPTION}    ${EMPTY}
    [EW] Write Cell StartSummary To Excel    SummaryReport
    [Return]    ${RESULT}

END REPORT SET
    [EW] Write Cell EndSummary To Excel    SummaryReport
    Move File    ${Foldername}${/}${RESULT}.xlsx    ${Foldername}${/}${RESULT}_${EXECUTE_VERSION}_${EXECUTE_STATUS}.xlsx
    Move Directory    ${Foldername}    ${DIR}${/}TEST_RESULT${/}${RESULT}_${EXECUTE_VERSION}_${EXECUTE_STATUS}

ANDROID SET
    [Arguments]    ${Platform}    ${Version}    ${DevicesName}    ${Package}    ${Activity}    ${Resetapp}=true    ${Port}=4723    ${Automate}=JOOKUI
    ${Status}    ${Error}    Run Keyword And Ignore Error    Open Application    http://localhost:${Port}/wd/hub    platformName=${Platform}    platformVersion=${Version}    deviceName=${DevicesName}    appPackage=${Package}    appActivity=${Activity}    automationName=${Automate}    noReset=${Resetapp}
    Run Keyword If    "${Status}" == "PASS"    Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
    Set Suite Variable    ${STEP_STATUS}    ${Status}
    Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
    Run Keyword If    "${Status}" == "PASS"    Set Suite Variable    ${ERROR_DESCRIPTION}    None
    Comment    FOR    ${I}    IN    platformName=${Platform}    platformVersion=${Version}    deviceName=${DevicesName}
    Comment    \    Log To Console    ${I}
    Comment    END

iOS SET
    [Arguments]    ${Platform}    ${Version}    ${DevicesName}    ${Package}    ${Activity}    ${Resetapp}=true    ${Port}=4723    ${Automate}=XCUITest
    ${Status}    ${Error}    Run Keyword And Ignore Error    Open Application    http://localhost:${Port}/wd/hub    platformName=${Platform}    platformVersion=${Version}    udid=${DevicesName}    appPackage=${Package}    appActivity=${Activity}    automationName=${Automate}    noReset=${Resetapp}
    Set Suite Variable    ${STEP_STATUS}    ${Status}
    Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
    Comment    FOR    ${I}    IN    platformName=${Platform}    platformVersion=${Version}    udid=${DevicesName}
    Comment    \    Log To Console    ${I}
    Comment    END

GLOBAL SET
    Log to Console    CURDIR --> ${CURDIR}
    ${DIR}    Replace String    ${CURDIR}    Framework${/}KEYWORDS${/}GENERAL    Framework
    Log To Console    DIR --> ${DIR}
    Set Suite Variable    ${DIR}    ${DIR}
    Set Suite Variable    ${FileConfigPath}    ${DIR}${/}TEST_DATA${/}
    Set Suite Variable    ${PathExcelResult}    ${DIR}${/}TEST_DATA${/}
    Set Suite Variable    ${ConfigSheetName}    Template
    Set Suite Variable    ${InputSheetName}    INPUT
    Set Suite Variable    ${OuputSheetName}    OUTPUT
    Set Suite Variable    ${TEST_DATA}    ${EMPTY}
    Set Suite Variable    ${VERIFY_FLAG}    N
    Set Suite Variable    ${CAPTURE_FLAG}    N
    Set Suite Variable    ${CAPTURE_PATH}    ${EMPTY}
    ${FolderDateTime}    Get Current Date    result_format=%Y%m%d%H%M%S
    ${FolderScreenshotDate}    Convert Date    ${FolderDateTime}    result_format=%Y%m%d_%H%M%S
    Set Suite Variable    ${FOLDER_CAPTURE_DATE}    ${FolderScreenshotDate}
    Set Suite Variable    ${OUTPUTRESULTDATE}    ${FolderScreenshotDate}
    Set Suite Variable    ${CAPTURE_ROW}    1
    Set Suite Variable    ${RUNNING_STEP}    1
    Set Suite Variable    ${EXIST_FLAG}    N

WAIT UNIT PAGE DOES NOT CONTAIN
    [Arguments]    ${Element}    ${TimeOut}=60s
    Set Library Search Order    AppiumLibrary
    ${Return}    Run Keyword And Ignore Error    Wait Until Page Does Not Contain    ${Element}    ${TimeOut}
    [Return]    ${Return}

CAPTURE SCREEN SET
    Create Directory    ${CAPTURE_PATH}${/}${FOLDER_CAPTURE_DATE}
    Set Suite Variable    ${FOLDER_CAPTURE_DATE}    ${CAPTURE_PATH}${/}${FOLDER_CAPTURE_DATE}
    Log To Console    Create Directory --> ${FOLDER_CAPTURE_DATE}

ADD ZERO
    [Arguments]    ${runningStep}
    Set Suite Variable    ${RUNNING}    0
    Run Keyword If    len('${runningStep}') == 1    Set Suite Variable    ${RUNNING}    000${runningStep}
    Run Keyword If    len('${runningStep}') == 2    Set Suite Variable    ${RUNNING}    00${runningStep}
    Run Keyword If    len('${runningStep}') == 3    Set Suite Variable    ${RUNNING}    0${runningStep}
    Run Keyword If    len('${runningStep}') == 4    Set Suite Variable    ${RUNNING}    ${runningStep}
    ${Return}    Set Variable    ${RUNNING}
    [Return]    ${Return}

ADD ZERO FORMAT DATE
    [Arguments]    ${number}
    Set Suite Variable    ${FORMAT}    0
    Run Keyword If    len('${number}') == 1    Set Suite Variable    ${FORMAT}    0${number}
    Run Keyword If    len('${number}') == 2    Set Suite Variable    ${FORMAT}    ${number}
    Run Keyword If    len('${number}') == 4    Set Suite Variable    ${FORMAT}    ${number}
    ${Return}    Set Variable    ${FORMAT}
    [Return]    ${Return}

GET MONTH FULL NAME
    [Arguments]    ${month}
    Set Suite Variable    ${FULL_NAME}    0
    Run Keyword If    '${month}' == '01'    Set Suite Variable    ${FULL_NAME}    January
    Run Keyword If    '${month}' == '02'    Set Suite Variable    ${FULL_NAME}    February
    Run Keyword If    '${month}' == '03'    Set Suite Variable    ${FULL_NAME}    March
    Run Keyword If    '${month}' == '04'    Set Suite Variable    ${FULL_NAME}    April
    Run Keyword If    '${month}' == '05'    Set Suite Variable    ${FULL_NAME}    May
    Run Keyword If    '${month}' == '06'    Set Suite Variable    ${FULL_NAME}    June
    Run Keyword If    '${month}' == '07'    Set Suite Variable    ${FULL_NAME}    July
    Run Keyword If    '${month}' == '08'    Set Suite Variable    ${FULL_NAME}    August
    Run Keyword If    '${month}' == '09'    Set Suite Variable    ${FULL_NAME}    September
    Run Keyword If    '${month}' == '10'    Set Suite Variable    ${FULL_NAME}    October
    Run Keyword If    '${month}' == '11'    Set Suite Variable    ${FULL_NAME}    November
    Run Keyword If    '${month}' == '12'    Set Suite Variable    ${FULL_NAME}    December
    ${Return}    Set Variable    ${FULL_NAME}
    [Return]    ${Return}

START TRANSACTION TIME
    ${StartDate}    Get Current Date    result_format=%d/%m/%Y %H\:%M\:%S
    Log To Console    Summary Start Date --> ${StartDate}
    Set Suite Variable    ${START_TIME}    ${StartDate}

END TRANSACTION TIME
    ${EndDate}    Get Current Date    result_format=%d/%m/%Y %H\:%M\:%S
    Log To Console    Summary Start Date --> ${EndDate}
    Set Suite Variable    ${END_TIME}    ${EndDate}

RESULT FOR ETM
    ${CaptureZipPath}    Replace String    ${CURDIR}    Framework${/}KEYWORDS${/}GENERAL    Framework${/}CAPTURE_SCREEN
    ${Status}    ${MSG}    Run Keyword And Ignore Error    Should Exist    ${CaptureZipPath}${/}Capture.zip
    Run Keyword If    '${Status}'=='PASS'    Remove File    ${CaptureZipPath}${/}Capture.zip
    Log To Console    Zip file Capture Screen -->${CaptureZipPath}${/}Capture.zip
    Create Zip From Files In Directory    ${FOLDER_CAPTURE_DATE}    ${CaptureZipPath}${/}Capture.zip    sub_directories=sub_directories
    ${Status}    ${MSG}    Run Keyword And Ignore Error    Should Exist    ${PathExcelResult}${/}${FolderData}${/}ExcelResult.zip
    Run Keyword If    '${Status}'=='PASS'    Remove Files    ${PathExcelResult}${/}${FolderData}${/}ExcelResult.zip
    Create Zip From Files In Directory    ${PathExcelResult}${/}${FolderData}${/}ExcelResult    ${PathExcelResult}${/}${FolderData}${/}ExcelResult.zip    sub_directories=sub_directories
    Log To Console    Zip file Excel result --> ${PathExcelResult}${/}${FolderData}${/}ExcelResult.zip
    ${Status}    ${MSG}    Run Keyword And Ignore Error    Should Exist    ${PathExcelResult}${/}${FolderData}${/}ExcelResult
    Run Keyword If    '${Status}'=='PASS'    Remove Directory    ${PathExcelResult}${/}${FolderData}${/}ExcelResult    Remove Directory

EXPORT EXCEL RESULT
    Comment    ${Status}    ${MSG}    Run Keyword And Ignore Error    Should Exist    ${PathExcelResult}${/}${FolderData}${/}ExcelResult
    Comment    Run Keyword If    '${Status}'=='PASS'    Remove Directory    ${PathExcelResult}${/}${FolderData}${/}ExcelResult    Remove Directory
    Create Directory    ${PathExcelResult}${/}${FolderData}${/}ExcelResult
    Log    output file-->${OUTPUTFILERESULT}${OUTPUTRESULTDATE}.xlsx
    Copy File    ${TEST_DATA}    ${OUTPUTFILERESULT}_${OUTPUTRESULTDATE}.xlsx
    Copy File    ${OUTPUTFILERESULT}_${OUTPUTRESULTDATE}.xlsx    ${PathExcelResult}${/}${FolderData}${/}ExcelResult

GET MONTH NUMBER
    [Arguments]    ${month}
    Set Suite Variable    ${FULL_NAME}    0
    Run Keyword If    '${month}' == '01'    Set Suite Variable    ${FULL_NAME}    January
    Run Keyword If    '${month}' == '02'    Set Suite Variable    ${FULL_NAME}    February
    Run Keyword If    '${month}' == '03'    Set Suite Variable    ${FULL_NAME}    March
    Run Keyword If    '${month}' == '04'    Set Suite Variable    ${FULL_NAME}    April
    Run Keyword If    '${month}' == '05'    Set Suite Variable    ${FULL_NAME}    May
    Run Keyword If    '${month}' == '06'    Set Suite Variable    ${FULL_NAME}    June
    Run Keyword If    '${month}' == '07'    Set Suite Variable    ${FULL_NAME}    July
    Run Keyword If    '${month}' == '08'    Set Suite Variable    ${FULL_NAME}    August
    Run Keyword If    '${month}' == '09'    Set Suite Variable    ${FULL_NAME}    September
    Run Keyword If    '${month}' == '10'    Set Suite Variable    ${FULL_NAME}    October
    Run Keyword If    '${month}' == '11'    Set Suite Variable    ${FULL_NAME}    November
    Run Keyword If    '${month}' == '12'    Set Suite Variable    ${FULL_NAME}    December
    ${Return}    Set Variable    ${FULL_NAME}
    [Return]    ${Return}
