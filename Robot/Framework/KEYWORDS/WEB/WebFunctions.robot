*** Settings ***
Resource          ../../SETTINGS/StandardLibrary.robot
Resource          ../../SETTINGS/AllResource.robot

*** Keywords ***
[W] Capture Page Screenshot
    [Arguments]    ${FOLDER_FUNCTION_NAME}    ${TRANS_NAME}
    Set Library Search Order    SeleniumLibrary
    ${DateTime}    Get Current Date    result_format=%Y%m%d%H%M%S%f
    ${ScreenshotDate}    Convert Date    ${DateTime}    result_format=%Y%m%d%H%M%S
    Log To Console    Screenshot Date --> ${ScreenshotDate}
    ${FOLDER_ROW}    ADD ZERO    ${CAPTURE_ROW}
    ${RUNNING}    ADD ZERO    ${RUNNING_STEP}
    ${OutputScreenshot}    Set Variable    ${FOLDER_CAPTURE_DATE}${/}${FOLDER_FUNCTION_NAME}${/}${FOLDER_ROW}${/}${RUNNING}_${TRANS_NAME}_${ScreenshotDate}.png
    Sleep    1s
    Capture Page Screenshot    ${OutputScreenshot}
    Set Suite Variable    ${RUNNING_STEP}    ${${RUNNING_STEP} + 1}
    Set Suite Variable    ${CAPTURE_SCREEN_SHOT}    ${FOLDER_CAPTURE_DATE}${/}${FOLDER_FUNCTION_NAME}${/}${FOLDER_ROW}
    [Return]    ${OutputScreenshot}

[W] Check Element
    [Arguments]    ${stepName}    ${ElementCheck}    ${timeout}=10s
    Set Library Search Order    SeleniumLibrary
    Comment    KEYWORD STATUS PENDING    ${stepName}
    Sleep    3s
    ${Status}    ${Error}    Run Keyword And Ignore Error    Wait Until Page Contains Element    ${ElementCheck}    timeout=${timeout}
    Set Suite Variable    ${TS_STATUS}    ${Status}
    Set Suite Variable    ${TS_MESSAGE}    ${Error}
    Log To Console    ${stepName} Status : ${TS_STATUS}
    Log To Console    ${stepName} Error : ${TS_MESSAGE}
    Comment    ${ScreenShot}    [W] Capture Page Screenshot
    Comment    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]

[W] Check Message
    [Arguments]    ${stepName}    ${Value}    ${timeout}=3s
    Set Library Search Order    SeleniumLibrary
    Comment    KEYWORD STATUS PENDING    ${stepName}
    Sleep    3s
    ${Status}    ${Error}    Run Keyword And Ignore Error    Wait Until Page Contains    ${Value}    timeout=${timeout}
    Set Suite Variable    ${TS_STATUS}    ${Status}
    Set Suite Variable    ${TS_MESSAGE}    ${Error}
    Log To Console    ${stepName} Status : ${TS_STATUS}
    Log To Console    ${stepName} Error : ${TS_MESSAGE}
    Comment    ${ScreenShot}    [W] Capture Page Screenshot
    Comment    KEYWORD STATUS END    ${ScreenShot}

[W] Click Element
    [Arguments]    ${stepName}    ${ElementClick}
    Set Library Search Order    SeleniumLibrary
    Comment    KEYWORD STATUS PENDING    ${stepName}
    Sleep    1s
    WAIT FOR PAGE CONTAINS ELEMENT    ${ElementClick}
    ${Status}    ${Error}    Run Keyword And Ignore Error    Click Element    ${ElementClick}
    Set Suite Variable    ${TS_STATUS}    ${Status}
    Set Suite Variable    ${TS_MESSAGE}    ${Error}
    Log To Console    ${stepName} Status : ${TS_STATUS}
    Log To Console    ${stepName} Error : ${TS_MESSAGE}
    Comment    ${ScreenShot}    [W] Capture Page Screenshot
    Comment    KEYWORD STATUS END    ${ScreenShot}

[W] Click Element Position
    [Arguments]    ${stepName}    ${ElementClick}    ${Position}
    Set Library Search Order    SeleniumLibrary
    KEYWORD STATUS PENDING    ${stepName}
    ${Sessions}    Get WebElements    ${ElementClick}
    Insert Into List    ${Sessions}    0    ${EMPTY}
    ${Status}    ${Error}    Run Keyword And Ignore Error    Click Element    @{Sessions}[${Position}]
    Set Suite Variable    ${STEP_STATUS}    ${Status}
    Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
    Log To Console    ${stepName} Status : ${STEP_STATUS}
    Log To Console    ${stepName} Error : ${ERROR_DESCRIPTION}
    ${ScreenShot}    [W] Capture Page Screenshot
    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]    KEYWORD STATUS TEARDOWN

[W] Double Click Element
    [Arguments]    ${stepName}    ${ElementDoubleClick}
    Set Library Search Order    SeleniumLibrary
    KEYWORD STATUS PENDING    ${stepName}
    WAIT FOR PAGE CONTAINS ELEMENT    ${ElementDoubleClick}
    ${Status}    ${Error}    Run Keyword And Ignore Error    Double Click Element    ${ElementDoubleClick}
    Set Suite Variable    ${STEP_STATUS}    ${Status}
    Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
    Log To Console    ${stepName} Status : ${STEP_STATUS}
    Log To Console    ${stepName} Error : ${ERROR_DESCRIPTION}
    ${ScreenShot}    [W] Capture Page Screenshot
    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]    KEYWORD STATUS TEARDOWN

[W] Double Click Element Position
    [Arguments]    ${stepName}    ${ElementDoubleClick}    ${Position}
    Set Library Search Order    SeleniumLibrary
    KEYWORD STATUS PENDING    ${stepName}
    ${Sessions}    Get WebElements    ${ElementDoubleClick}
    Insert Into List    ${Sessions}    0    ${EMPTY}
    ${Status}    ${Error}    Run Keyword And Ignore Error    Double Click Element    @{Sessions}[${Position}]
    Set Suite Variable    ${STEP_STATUS}    ${Status}
    Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
    Log To Console    ${stepName} Status : ${STEP_STATUS}
    Log To Console    ${stepName} Error : ${ERROR_DESCRIPTION}
    ${ScreenShot}    [W] Capture Page Screenshot
    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]    KEYWORD STATUS TEARDOWN

[W] Enter
    [Arguments]    ${stepName}    ${ElementEnter}
    Set Library Search Order    SeleniumLibrary
    Comment    KEYWORD STATUS PENDING    ${stepName}
    WAIT FOR PAGE CONTAINS ELEMENT    ${ElementEnter}
    Press Key    ${ElementEnter}    \\13
    Comment    KEYWORD STATUS END    None
    [Teardown]    KEYWORD STATUS TEARDOWN

[W] Get Text
    [Arguments]    ${ElementGetText}
    Set Library Search Order    SeleniumLibrary
    Comment    KEYWORD STATUS PENDING    [W] Get Text
    WAIT FOR PAGE CONTAINS ELEMENT    ${ElementGetText}
    ${Return}    Get Text    ${ElementGetText}
    Comment    KEYWORD STATUS END    None
    [Teardown]
    [Return]    ${Return}

[W] Get Text Position
    [Arguments]    ${ElementGetText}    ${Position}
    Set Library Search Order    SeleniumLibrary
    Comment    KEYWORD STATUS PENDING    [W] Get Text Position
    ${Sessions}    Get WebElements    ${ElementGetText}
    Insert Into List    ${Sessions}    0    ${EMPTY}
    ${Return}    Get Text    @{Sessions}[${Position}]
    Comment    KEYWORD STATUS END    None
    [Teardown]    KEYWORD STATUS TEARDOWN
    [Return]    ${Return}

[W] Get Value
    [Arguments]    ${ElementGetValue}
    Set Library Search Order    SeleniumLibrary
    Comment    KEYWORD STATUS PENDING    [W] Get Value
    WAIT FOR PAGE CONTAINS ELEMENT    ${ElementGetValue}
    ${Return}    Get Value    ${ElementGetValue}
    Comment    KEYWORD STATUS END    None
    [Teardown]    KEYWORD STATUS TEARDOWN
    [Return]    ${Return}

[W] Get Value Position
    [Arguments]    ${ElementGetValue}    ${Position}
    Set Library Search Order    SeleniumLibrary
    Comment    KEYWORD STATUS PENDING    [W] Get Value Position
    ${Sessions}    Get WebElements    ${ElementGetValue}
    Insert Into List    ${Sessions}    0    ${EMPTY}
    ${Return}    Get Value    @{Sessions}[${Position}]
    Comment    KEYWORD STATUS END    None
    [Teardown]    KEYWORD STATUS TEARDOWN
    [Return]    ${Return}

[W] Go Back
    [Arguments]    ${stepName}
    Set Library Search Order    SeleniumLibrary
    KEYWORD STATUS PENDING    ${stepName}
    ${Status}    ${Error}    Run Keyword And Ignore Error    Go Back
    Set Suite Variable    ${STEP_STATUS}    ${Status}
    Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
    Log To Console    ${stepName} Status : ${STEP_STATUS}
    Log To Console    ${stepName} Error : ${ERROR_DESCRIPTION}
    ${ScreenShot}    [W] Capture Page Screenshot
    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]    KEYWORD STATUS TEARDOWN

[W] Input Password
    [Arguments]    ${stepName}    ${ElementInputPassword}    ${Value}
    Set Library Search Order    SeleniumLibrary
    Comment    KEYWORD STATUS PENDING    ${stepName}
    Sleep    1s
    WAIT FOR PAGE CONTAINS ELEMENT    ${ElementInputPassword}
    ${Status}    ${Error}    Run Keyword And Ignore Error    Input Password    ${ElementInputPassword}    ${Value}
    Set Suite Variable    ${TS_STATUS}    ${Status}
    Set Suite Variable    ${TS_MESSAGE}    ${Error}
    Log To Console    ${stepName} Status : ${TS_STATUS}
    Log To Console    ${stepName} Error : ${TS_MESSAGE}
    Comment    ${ScreenShot}    [W] Capture Page Screenshot
    Comment    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]

[W] Input Text
    [Arguments]    ${stepName}    ${ElementInputText}    ${Value}
    Set Library Search Order    SeleniumLibrary
    Comment    KEYWORD STATUS PENDING    ${stepName}
    Sleep    1s
    WAIT FOR PAGE CONTAINS ELEMENT    ${ElementInputText}
    ${Status}    ${Error}    Run Keyword And Ignore Error    Input Text    ${ElementInputText}    ${Value}
    Set Suite Variable    ${TS_STATUS}    ${Status}
    Set Suite Variable    ${TS_MESSAGE}    ${Error}
    Log To Console    ${stepName} Status : ${TS_STATUS}
    Log To Console    ${stepName} Error : ${TS_MESSAGE}
    Comment    ${ScreenShot}    [W] Capture Page Screenshot
    Comment    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]

[W] Input Manual
    [Arguments]    ${stepName}    ${ElementInputText}    ${Expected}
    Set Library Search Order    SeleniumLibrary
    Comment    KEYWORD STATUS PENDING    ${stepName}
    WAIT FOR PAGE CONTAINS ELEMENT    ${ElementInputText}
    ${ManualInput}    Get Value From User    Input value
    ${Status}    ${Error}    Run Keyword And Ignore Error    Input Text    ${ElementInputText}    ${ManualInput}
    Set Suite Variable    ${STEP_STATUS}    ${Status}
    Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
    Log To Console    ${stepName} Status : ${STEP_STATUS}
    Log To Console    ${stepName} Error : ${ERROR_DESCRIPTION}
    Comment    ${ScreenShot}    [W] Capture Page Screenshot
    Comment    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]

[W] Mouse Over
    [Arguments]    ${ElementMouseOver}
    Set Library Search Order    SeleniumLibrary
    Comment    KEYWORD STATUS PENDING    [W] Mouse Over
    WAIT FOR PAGE CONTAINS ELEMENT    ${ElementMouseOver}
    Mouse Over    ${ElementMouseOver}
    Comment    KEYWORD STATUS END    None
    [Teardown]    KEYWORD STATUS TEARDOWN

[W] Open Browser
    [Arguments]    ${stepName}    ${URL}    ${Browser}=Chrome
    Set Library Search Order    SeleniumLibrary
    Comment    KEYWORD STATUS PENDING    ${stepName}
    ${Status}    ${Error}    Run Keyword And Ignore Error    Open Browser    ${URL}    ${Browser}
    Set Suite Variable    ${TS_STATUS}    ${Status}
    Set Suite Variable    ${TS_MESSAGE}    ${Error}
    FOR    ${i}    IN    1
        ${Status}    ${Error}    Run Keyword And Ignore Error    Maximize Browser Window
        Set Suite Variable    ${TS_STATUS}    ${Status}
        Set Suite Variable    ${TS_MESSAGE}    ${Error}
    END
    Log To Console    ${stepName} Status : ${TS_STATUS}
    Log To Console    ${stepName} Error : ${TS_MESSAGE}
    Comment    ${ScreenShot}    [W] Capture Page Screenshot
    Comment    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]

[W] Select Frame
    [Arguments]    ${stepName}    @{Element}
    Set Library Search Order    SeleniumLibrary
    KEYWORD STATUS PENDING    ${stepName}
    Unselect Frame
    FOR    ${i}    IN    @{Element}
        WAIT FOR PAGE CONTAINS ELEMENT    ${i}
        Select Frame    ${i}
    END
    Comment    ${ScreenShot}    [W] Capture Page Screenshot
    Comment    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]    KEYWORD STATUS TEARDOWN

[W] Select From List By Value
    [Arguments]    ${stepName}    ${Element}    ${Value}
    Set Library Search Order    SeleniumLibrary
    Comment    KEYWORD STATUS PENDING    ${stepName}
    Sleep    3s
    WAIT FOR PAGE CONTAINS ELEMENT    ${Element}
    ${Status}    ${Error}    Run Keyword And Ignore Error    Select From List By Label    ${Element}    ${Value}
    Run Keyword If    "${Status}" != "PASS"    Select From List By Value    ${Element}    ${Value}
    Set Suite Variable    ${TS_STATUS}    ${Status}
    Set Suite Variable    ${TS_MESSAGE}    ${Error}
    Log To Console    ${stepName} Result Get Text --> ${Value}
    Log To Console    ${stepName} Status : ${TS_STATUS}
    Log To Console    ${stepName} Error : ${TS_MESSAGE}
    Comment    ${ScreenShot}    [W] Capture Page Screenshot
    Comment    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]

[W] Close Browser
    [Arguments]    ${stepName}
    Set Library Search Order    SeleniumLibrary
    Comment    KEYWORD STATUS PENDING    ${stepName}
    ${Status}    ${Error}    Run Keyword And Ignore Error    Close All Browsers
    Set Suite Variable    ${TS_STATUS}    ${Status}
    Set Suite Variable    ${TS_MESSAGE}    ${Error}
    Log To Console    ${stepName} Status : ${TS_STATUS}
    Log To Console    ${stepName} Error : ${TS_MESSAGE}
    Comment    KEYWORD STATUS END    None
    [Teardown]    KEYWORD STATUS TEARDOWN

[W] Switch To Window
    [Arguments]    ${TAB}=1
    Set Library Search Order    SeleniumLibrary
    KEYWORD STATUS PENDING    [W] Switch To Window
    ${Windows}    Get Window Handles
    Insert Into List    ${Windows}    0    ${EMPTY}
    FOR    ${i}    IN    @{Windows}[${TAB}]
        Switch Window    ${i}
    END
    ${ScreenShot}    [W] Capture Page Screenshot
    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]    KEYWORD STATUS TEARDOWN

[W] Verify Result
    [Arguments]    ${stepName}    ${Value}    ${Expected}
    Set Library Search Order    SeleniumLibrary
    KEYWORD STATUS PENDING    ${stepName}
    Log To Console    Value --> ${Value}
    Log To Console    Expected --> ${Expected}
    Log To Console    Value --> ${Value}
    FOR    ${i}    IN    1
        ${Status}    ${Error}    Run Keyword And Ignore Error    Should Be Equal As Strings    "${Value}"    "${Expected}"
        Set Suite Variable    ${STEP_STATUS}    ${Status}
        Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
        Run Keyword If    "${Status}" != "PASS"    [W] Replace Error Message    ${Status}    ${Value}    ${Expected}
    END
    Log To Console    ${stepName} Status : ${STEP_STATUS}
    Log To Console    ${stepName} Error : ${ERROR_DESCRIPTION}
    ${ScreenShot}    [W] Capture Page Screenshot
    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]    KEYWORD STATUS TEARDOWN

[W] Mouse Down
    [Arguments]    ${Element}
    Set Library Search Order    SeleniumLibrary
    Comment    KEYWORD STATUS PENDING    [W] Mouse Over
    WAIT FOR PAGE CONTAINS ELEMENT    ${Element}
    Mouse Down    ${Element}
    Comment    KEYWORD STATUS END    None
    [Teardown]    KEYWORD STATUS TEARDOWN

[W] Click Element and Verify Result
    [Arguments]    ${stepName}    ${ElementClick}    ${ElementGetText}    ${Expected}
    Set Library Search Order    SeleniumLibrary
    KEYWORD STATUS PENDING    ${stepName}
    WAIT FOR PAGE CONTAINS ELEMENT    ${ElementClick}
    ${Status}    ${Error}    Run Keyword And Ignore Error    Click Element    ${ElementClick}
    Set Suite Variable    ${STEP_STATUS}    ${Status}
    Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
    ${result}    [W] Get Text    ${ElementGetText}
    Log To Console    ${stepName} Result Get Text --> ${result}
    FOR    ${i}    IN    1    1
        ${Status}    ${Error}    Run Keyword And Ignore Error    Should Be Equal As Strings    "${result}"    "${Expected}"
        Set Suite Variable    ${STEP_STATUS}    ${Status}
        Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
        Run Keyword If    "${Status}" != "PASS"    [W] Replace Error Message    ${Status}    ${result}    ${Expected}
    END
    Log To Console    ${stepName} Status : ${STEP_STATUS}
    Log To Console    ${stepName} Error : ${ERROR_DESCRIPTION}
    ${ScreenShot}    [W] Capture Page Screenshot
    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]    KEYWORD STATUS TEARDOWN

[W] Input Text and Verify Result
    [Arguments]    ${stepName}    ${ElementInputText}    ${ElementGetValue}    ${Value}    ${Expected}
    Set Library Search Order    SeleniumLibrary
    KEYWORD STATUS PENDING    ${stepName}
    WAIT FOR PAGE CONTAINS ELEMENT    ${ElementInputText}
    ${Status}    ${Error}    Run Keyword And Ignore Error    Input Text    ${ElementInputText}    ${Value}
    Set Suite Variable    ${STEP_STATUS}    ${Status}
    Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
    ${result}    [W] Get Value    ${ElementGetValue}
    Log To Console    ${stepName} Result Get Text --> ${result}
    Comment    ${result}    Replace String    ${result}    ,    ${EMPTY}
    Comment    ${Expected}    Replace String    ${Expected}    ,    ${EMPTY}
    FOR    ${i}    IN    1    1
        ${Status}    ${Error}    Run Keyword And Ignore Error    Should Be Equal As Strings    "${result}"    "${Expected}"
        Set Suite Variable    ${STEP_STATUS}    ${Status}
        Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
        Run Keyword If    "${Status}" != "PASS"    [W] Replace Error Message    ${Status}    ${result}    ${Expected}
    END
    Log To Console    ${stepName} Status : ${STEP_STATUS}
    Log To Console    ${stepName} Error : ${ERROR_DESCRIPTION}
    ${ScreenShot}    [W] Capture Page Screenshot
    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]    KEYWORD STATUS TEARDOWN

[W] Replace Error Message
    [Arguments]    ${Status}    ${Value}    ${Expected}
    Log To Console    Replace Error Message
    Set Suite Variable    ${STEP_STATUS}    ${Status}
    Set Suite Variable    ${ERROR_DESCRIPTION}    ข้อมูลที่ส่งเข้ามา ${Value} ไม่ตรงกับ ${Expected}

[W] Get Element Count
    [Arguments]    ${stepName}    ${ElementGet}
    Set Library Search Order    SeleniumLibrary
    Comment    KEYWORD STATUS PENDING    ${stepName}
    Sleep    3s
    WAIT FOR PAGE CONTAINS ELEMENT    ${ElementGet}
    ${Return}    Get Element Count    ${ElementGet}
    Comment    Set Suite Variable    ${TS_STATUS}    ${Status}
    Comment    Set Suite Variable    ${TS_MESSAGE}    ${Error}
    Comment    Log To Console    ${stepName} Status : ${TS_STATUS}
    Comment    Log To Console    ${stepName} Error : ${TS_MESSAGE}
    Comment    ${ScreenShot}    [W] Capture Page Screenshot
    Comment    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]
    [Return]    ${Return}
