*** Settings ***
Resource          ../../SETTINGS/StandardLibrary.robot
Resource          ../../SETTINGS/AllResource.robot

*** Keywords ***
[EW] Write Cell ISO Auth To Excel
    [Arguments]    ${SheetName}    ${Row}    ${PrimaryAccountNumber}    ${FixFild}    ${Terminal}    ${Merchant}    ${AmountTransaction}    ${CalChipData}    ${ChipData}    ${ServiceCode}    ${Pin}    ${CVV}    ${Track2}    ${ExpireDate}
    Set Library Search Order    ExcelRobot
    Log to Console    Open Excel To Write ISO Auth # --> ${TEST_DATA}
    Open Excel To Write    ${TEST_DATA}
    Write To Cell By Name    ${SheetName}    A${${Row}+1}    ${TS_STATUS}
    Run Keyword If    "${TS_MESSAGE}" == "None"    Write To Cell By Name    ${SheetName}    B${${Row}+1}    ${EMPTY}
    Run Keyword If    "${TS_MESSAGE}" != "None"    Write To Cell By Name    ${SheetName}    B${${Row}+1}    ${TS_MESSAGE}
    Write To Cell By Name    ${SheetName}    C${${Row}+1}    ${START_TIME}
    END TRANSACTION TIME
    Run Keyword If    "${START_TIME}" != ""    Write To Cell By Name    ${SheetName}    D${${Row}+1}    ${END_TIME}
    Write To Cell By Name    ${SheetName}    E${${Row}+1}    ${PrimaryAccountNumber}
    Write To Cell By Name    ${SheetName}    F${${Row}+1}    ${FixFild}
    Write To Cell By Name    ${SheetName}    G${${Row}+1}    ${Terminal}
    Write To Cell By Name    ${SheetName}    H${${Row}+1}    ${Merchant}
    Write To Cell By Name    ${SheetName}    I${${Row}+1}    ${AmountTransaction}
    Write To Cell By Name    ${SheetName}    J${${Row}+1}    ${CalChipData}
    Write To Cell By Name    ${SheetName}    K${${Row}+1}    ${ChipData}
    Write To Cell By Name    ${SheetName}    L${${Row}+1}    ${ServiceCode}
    Write To Cell By Name    ${SheetName}    M${${Row}+1}    ${Pin}
    Write To Cell By Name    ${SheetName}    N${${Row}+1}    ${CVV}
    Write To Cell By Name    ${SheetName}    O${${Row}+1}    ${Track2}
    Write To Cell By Name    ${SheetName}    P${${Row}+1}    ${ExpireDate}
    Save Excel
    Set Suite Variable    ${AddRow}    ${${Row} + 1}
    Set Suite Variable    ${START_TIME}    ${EMPTY}
    Set Suite Variable    ${END_TIME}    ${EMPTY}

[EW] Write Cell ISO Clear Rows Data To Excel
    [Arguments]    ${SheetName}
    Set Library Search Order    ExcelRobot
    Log to Console    Open Excel To Write KBOLA Clear Rows Data # --> ${TEST_DATA}
    ${CountColumn}    ${CountRow}    [ER] Open Excel    ${TEST_DATA}    ${SheetName}
    Log to Console    CountColumn --> ${CountColumn}
    Log to Console    CountRow --> ${CountRow}
    Open Excel To Write    ${TEST_DATA}
    FOR    ${i}    IN RANGE    1    ${${CountRow} + 1}
        [EW] Write Cell ISO Clear Columns Data To Excel    ${SheetName}    ${i}    ${CountColumn}
    END
    Save Excel

[EW] Write Cell ISO Clear Columns Data To Excel
    [Arguments]    ${SheetName}    ${Row}    ${CountColumn}
    Set Library Search Order    ExcelRobot
    Log to Console    Open Excel To Write ISO Clear Columns Data # --> ${TEST_DATA} ${SheetName} ${Row}
    FOR    ${i}    IN RANGE    0    ${CountColumn}
        Log To Console    Get Data & Clear [${Row}][${i}]
        Write To Cell    ${SheetName}    ${i}    ${Row}    ${EMPTY}
    END
