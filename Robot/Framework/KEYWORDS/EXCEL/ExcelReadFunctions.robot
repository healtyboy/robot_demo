*** Settings ***
Resource          ../../SETTINGS/StandardLibrary.robot
Resource          ../../SETTINGS/AllResource.robot

*** Keywords ***
[ER] Get Data Row For Excel
    [Arguments]    ${Filename}    ${SheetName}    ${Row}
    Set Library Search Order    ExcelRobot
    ${CountColumn}    ${CountRow}    [ER] Open Excel    ${Filename}    ${SheetName}
    ${Row}    Evaluate    ${Row}-1
    FOR    ${i}    IN    ${Row}
        ${Return}    [ER] Read Cell Data Column For Excel    ${SheetName}    ${CountColumn}    ${i}
    END
    Insert Into List    ${Return}    0    ${EMPTY}
    Set Suite Variable    ${STEP_STATUS}    PASS
    [Teardown]
    [Return]    ${Return}

[ER] Open Excel
    [Arguments]    ${Filename}    ${SheetName}
    Set Library Search Order    ExcelRobot
    ${Filename}    Join Path    ${Filename}
    Comment    ${DIR}    Remove String    ${CURDIR}    Keywork
    ${DIR}    Set Variable    ${Filename}
    Open Excel    ${DIR}
    ${CountColumn}    Get Column Count    ${SheetName}
    ${CountRow}    Get Row Count    ${SheetName}
    [Return]    ${CountColumn}    ${CountRow}

[ER] Read Cell Data Column For Excel
    [Arguments]    ${SheetName}    ${CountColumn}    ${Row}    &{DictionaryData}
    Set Library Search Order    ExcelRobot
    FOR    ${i}    IN RANGE    ${CountColumn}
        ${Key}    Read Cell Data    ${SheetName}    ${i}    0
        ${Value}    Read Cell Data    ${SheetName}    ${i}    ${Row}
        Log To Console    Key -- > ${Key}
        Log To Console    Value --> ${Value}
        Run Keyword If    '${Key}' == '' and '${Value}' == ''    Continue For Loop
        Set To Dictionary    ${DictionaryData}    ${Key}=${Value}
    END
    [Return]    &{DictionaryData}

[ER] Get Path Test Data on Configuration
    [Arguments]    ${Filename}    ${SheetName}    ${ScriptName}
    Set Library Search Order    ExcelRobot
    Log To Console    File Name --> ${Filename}
    ${CountColumn}    ${CountRow}    [ER] Open Excel    ${Filename}    ${SheetName}
    &{TEST_DATA_DIC}    Create Dictionary    KEY=VALUE
    @{TEST_EXECUTE}    Create List
    FOR    ${i}    IN RANGE    1    ${CountRow}
        ${Return}    [ER] Read Cell Data Column For Excel    ${SheetName}    ${CountColumn}    ${i}    &{TEST_DATA_DIC}
        Log To Console    Return --> ${Return}
        Append To List    ${TEST_EXECUTE}    ${Return}
    END
    Log To Console    List Script Name --> ${TEST_EXECUTE}
    Comment    ${TestCase}    Replace String    ${TestName}    ${SPACE}    _
    Comment    ${TestCase}    Catenate    SEPARATOR=    ROBOT_S_    ${TestCase}
    Log To Console    ${ScriptName}
    FOR    ${j}    IN    @{TEST_EXECUTE}
        ${Name}    Get From Dictionary    ${j}    SCRIPT_NAME
        ${TestData}    Get From Dictionary    ${j}    TEST_DATA
        ${OutputfileData}    Remove String    ${TestData}    .xlsx
        Log To Console    ${OutputfileData}
        ${VerifyFlag}    Get From Dictionary    ${j}    VERIFY
        ${CaptureFlag}    Get From Dictionary    ${j}    CAPTURE
        ${CapturePath}    Get From Dictionary    ${j}    CAPTURE_PATH
        Run Keyword If    "${Name}" != "${ScriptName}"    Continue For Loop
        Log To Console    Used --> ${TestData}
        Set Suite Variable    ${TEST_DATA}    ${TestData}
        Set Suite Variable    ${VERIFY_FLAG}    ${VerifyFlag}
        Set Suite Variable    ${CAPTURE_FLAG}    ${CaptureFlag}
        Set Suite Variable    ${CAPTURE_PATH}    ${CapturePath}
        Set Suite Variable    ${OUTPUTFILERESULT}    ${OutputfileData}
    END

[ER] Get Data All Row For Excel
    [Arguments]    ${Filename}    ${SheetName}
    Set Library Search Order    ExcelRobot
    ${CountColumn}    ${CountRow}    [ER] Open Excel    ${Filename}    ${SheetName}
    &{ReturnAllDicData}    Create Dictionary    KEY=VALUE
    ${ReturnAllListData}    Create List
    FOR    ${i}    IN RANGE    1    ${CountRow}
        ${Return}    [ER] Read Cell Data Column For Excel    ${SheetName}    ${CountColumn}    ${i}    &{ReturnAllDicData}
        Append To List    ${ReturnAllListData}    ${Return}
    END
    Insert Into List    ${ReturnAllListData}    0    ${EMPTY}
    Set Suite Variable    ${STEP_STATUS}    PASS
    [Teardown]
    [Return]    ${ReturnAllListData}

[ER] Set Dictionary
